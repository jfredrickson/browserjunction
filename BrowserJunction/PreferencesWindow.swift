// BrowserJunction
// Copyright © 2019 Jeff Fredrickson
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Cocoa

class PreferencesWindow: NSWindowController {
    var browserManager: BrowserManager?
    @IBOutlet weak var defaultBrowserButton: NSPopUpButton!
    @IBOutlet var browserListController: NSDictionaryController!
    @IBOutlet var scriptTextView: NSTextView!

    override var windowNibName: String! {
        return "PreferencesWindow"
    }

    override func windowDidLoad() {
        super.windowDidLoad()
        configureDefaultBrowserButton()
        configureScriptTextView()
        show()
    }

    func show() {
        self.browserManager = BrowserManager()
        self.window?.center()
        self.window?.makeKeyAndOrderFront(nil)
        NSApplication.shared.activate(ignoringOtherApps: true)
    }

    func configureDefaultBrowserButton() {
        browserListController.content = browserManager?.browsers
        let sortAlpha = NSSortDescriptor(key: "value", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
        browserListController.sortDescriptors = [sortAlpha]
        if let defaultBrowser = browserManager?.defaultBrowser {
            if let browserName = browserManager?.browserName(forBundleIdentifier: defaultBrowser) {
                defaultBrowserButton.selectItem(withTitle: browserName)
            }
        }
        defaultBrowserButton.sizeToFit()
    }

    func configureScriptTextView() {
        let configTextFont = NSFont.userFixedPitchFont(ofSize: 0)
        scriptTextView.font = configTextFont
        scriptTextView.delegate = self
        scriptTextView.isAutomaticQuoteSubstitutionEnabled = false
        scriptTextView.isAutomaticDashSubstitutionEnabled = false
        scriptTextView.isAutomaticTextReplacementEnabled = false
        if let script = browserManager?.script {
            scriptTextView.string = script
        }
    }

    @IBAction func handleDefaultBrowserButtonChange(_ sender: NSPopUpButton) {
        if let selectedBrowser = browserListController.selectedObjects.first as? NSDictionaryControllerKeyValuePair {
            if let selectedBrowserIdentifier = selectedBrowser.key {
                browserManager?.defaultBrowser = selectedBrowserIdentifier
            }
        }
    }
}

extension PreferencesWindow: NSTextViewDelegate {
    func textDidChange(_ notification: Notification) {
        browserManager?.script = scriptTextView.string
    }
}
