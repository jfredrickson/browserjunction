// BrowserJunction
// Copyright © 2019 Jeff Fredrickson
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    let eventManager = NSAppleEventManager.shared()
    let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
    @IBOutlet weak var appMenuItem: NSMenuItem!
    let preferencesWindow = PreferencesWindow()

    func applicationWillFinishLaunching(_ notification: Notification) {
        eventManager.setEventHandler(self, andSelector: #selector(handleUrlEvent(_:withReplyEvent:)), forEventClass: AEEventClass(kInternetEventClass), andEventID: AEEventID(kAEGetURL))
    }

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        configureMenu()
    }

    @objc func handleUrlEvent(_ event: NSAppleEventDescriptor, withReplyEvent replyEvent: NSAppleEventDescriptor) {
        guard let url = URL(string: event.paramDescriptor(forKeyword: AEKeyword(keyDirectObject))!.stringValue!) else { return }
        guard let pid = event.attributeDescriptor(forKeyword: AEKeyword(keySenderPIDAttr))?.int32Value else { return }
        let sourceBundleIdentifier = NSRunningApplication(processIdentifier: pid)?.bundleIdentifier
        let browserManager = BrowserManager()
        browserManager.launch(url, source: sourceBundleIdentifier)
    }

    func configureMenu() {
        statusItem.menu = appMenuItem.submenu
        if let button = statusItem.button {
            let image = NSImage(named: "StatusBarIcon")
            image?.size = NSSize(width: 12, height: 12)
            button.image = image
        }
    }

    @IBAction func showAbout(_ sender: Any) {
        NSApp.activate(ignoringOtherApps: true)
        NSApp.orderFrontStandardAboutPanel(sender)
    }

    @IBAction func showPreferences(_ sender: Any) {
        NSApp.activate(ignoringOtherApps: true)
        preferencesWindow.show()
    }
}
