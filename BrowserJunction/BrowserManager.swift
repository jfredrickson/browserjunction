// BrowserJunction
// Copyright © 2019 Jeff Fredrickson
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Cocoa
import JavaScriptCore
import os.log

class BrowserManager: NSObject {
    let fallbackBrowser = "com.apple.Safari"

    var defaultBrowser: String {
        get {
            return UserDefaults.standard.string(forKey: "defaultBrowserIdentifier") ?? fallbackBrowser
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "defaultBrowserIdentifier")
        }
    }

    var script: String {
        get {
            return UserDefaults.standard.string(forKey: "script") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "script")
        }
    }

    var browsers: [String: String] {
        var browsers = [String: String]()
        let browserIdentifiers = LSCopyAllHandlersForURLScheme("http" as CFString)?.takeUnretainedValue() as! Array<String>
        for browserIdentifier in browserIdentifiers {
            browsers[browserIdentifier] = browserName(forBundleIdentifier: browserIdentifier)
        }
        return browsers
    }

    let context = JSContext()

    override init() {
        super.init()
        setupContext()
    }

    func setupContext() {
        // Handle JS exceptions.
        context?.exceptionHandler = { context, exception in
            self.log("Exception: \(String(describing: exception?.toString()))")
        }

        // Inject our own console object into the JS context so that we can access console.log() messages.
        let consoleLog: @convention(block) (String) -> Void = { message in
            self.log("Script output: \(message)")
        }
        context?.setObject(consoleLog, forKeyedSubscript: "_consoleLog" as (NSCopying & NSObjectProtocol))
        context?.evaluateScript("const console = { log: (message) => _consoleLog(message) }")
    }

    func browserName(forBundleIdentifier bundleIdentifier: String) -> String? {
        if let browserPath = NSWorkspace.shared.absolutePathForApplication(withBundleIdentifier: bundleIdentifier) {
            let fileName = (browserPath as NSString).lastPathComponent
            let appName = (fileName as NSString).deletingPathExtension
            return appName
        }
        return nil
    }

    func browserIcon(forBundleIdentifier bundleIdentifier: String) -> NSImage? {
        if let browserPath = NSWorkspace.shared.absolutePathForApplication(withBundleIdentifier: bundleIdentifier) {
            let icon = NSWorkspace.shared.icon(forFile: browserPath)
            return icon
        }
        return nil
    }

    func launch(_ url: URL, source: String?) {
        let browserIdentifiers = LSCopyAllHandlersForURLScheme("http" as CFString)?.takeUnretainedValue() as! Array<String>
        context?.setObject(browserIdentifiers, forKeyedSubscript: "browsers" as (NSCopying & NSObjectProtocol))
        context?.setObject(source, forKeyedSubscript: "source" as (NSCopying & NSObjectProtocol))

        var browser = defaultBrowser
        if let result = context?.evaluateScript(script)?.toString() {
            log("Script result: \(result)")
            if browserIdentifiers.contains(result) {
                browser = result
            }
        }

        NSWorkspace.shared.open([url], withAppBundleIdentifier: browser, options: .default, additionalEventParamDescriptor: nil, launchIdentifiers: nil)
    }

    func log(_ message: String) {
        let subsystem = Bundle.main.bundleIdentifier!
        let category = "Script"
        let log = OSLog(subsystem: subsystem, category: category)
        os_log(.info, log: log, "%@", message)
    }
}
